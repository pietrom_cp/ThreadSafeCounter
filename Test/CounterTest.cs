﻿using System;
using System.Threading;
using ThreadSafeCounter;
using Xunit;

namespace Test {
    public class CounterTest {
        private const int INITIAL_VALUE = 10;
        
        [Fact]
        public void CanIncrementCounterValue() {
            Counter counter = new Counter(INITIAL_VALUE);
            counter.Increment();
            Assert.Equal(INITIAL_VALUE + 1, counter.Value);
        }
        
        [Fact]
        public void CanIncrementCounterValueMultipleTimes() {
            Counter counter = new Counter(INITIAL_VALUE);
            const int times = 19;
            for (int i = 0; i < times; i++) {
                counter.Increment();                
            }
            Assert.Equal(INITIAL_VALUE + times, counter.Value);
        }

        [Fact]
        public void DefaultInitialValueIsZero() {
            Counter counter = new Counter();
            Assert.Equal(0, counter.Value);
        }
        
        [Fact]
        public void CanUseTheSameCounterInMultipleConcurrentThreads() {
            Random random = new Random();
            Counter counter = new Counter();
            int threadCount = 100;
            int incrementCount = 100;
            Barrier startBarrier = new Barrier(threadCount);
            Barrier endBarrier = new Barrier(threadCount + 1);
            for (int i = 0; i < threadCount; i++) {
                new Thread(() => {
                    startBarrier.SignalAndWait();
                    for (int j = 0; j < incrementCount; j++) {
                        Thread.Sleep(random.Next(10));
                        counter.Increment();                        
                    }
                    endBarrier.SignalAndWait();
                }).Start();
            }
            endBarrier.SignalAndWait();
            Assert.Equal(threadCount * incrementCount, counter.Value);
        }
    }
}